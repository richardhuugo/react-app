$(function() {
  /** CADASTRO DE PRESTADORES SCRIPT */
  let navListItems = $('div.setup-panel div a');
    var allWells = $('.setup-content');
    var allNextBtn = $('.nextBtn');

  allWells.hide();

  navListItems.click(function(e) {
    e.preventDefault();
    let $target = $($(this).attr('href'));
      var $item = $(this);

    if (!$item.hasClass('disabled')) {
      navListItems.removeClass('btn-primary').addClass('btn-default');
      $item.addClass('btn-primary');
      allWells.hide();
      $target.show();
      $target.find('input:eq(0)').focus();
    }
  });

  allNextBtn.click(function() {
    let curStep = $(this).closest('.setup-content');
      var curStepBtn = curStep.attr('id');
      var nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]')
        .parent()
        .next()
        .children('a');
      var curInputs = curStep.find("input[type='text'],input[type='url']");
      var isValid = true;

    $('.form-group').removeClass('has-error');
    for (let i = 0; i < curInputs.length; i++) {
      if (!curInputs[i].validity.valid) {
        isValid = false;
        $(curInputs[i])
          .closest('.form-group')
          .addClass('has-error');
      }
    }

    if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel div a.btn-primary').trigger('click');
  /** CADASTRO DE PRESTADORES SCRIPT */
  function toggleFullScreen() {
    if (
      (document.fullScreenElement && document.fullScreenElement !== null) ||
      (!document.mozFullScreen && !document.webkitIsFullScreen)
    ) {
      if (document.documentElement.requestFullScreen) {
        document.documentElement.requestFullScreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullScreen) {
        document.documentElement.webkitRequestFullScreen(
          Element.ALLOW_KEYBOARD_INPUT
        );
      }
    } else if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
  }
});

jQuery(document).ready(function() {
  // cache DOM elements
  let mainContent = $('.cd-main-content');
    var header = $('.cd-main-header');
    var sidebar = $('.cd-side-nav');
    var sidebarTrigger = $('.cd-nav-trigger');
    var topNavigation = $('.cd-top-nav');
    var searchForm = $('.cd-search');
    var accountInfo = $('.account');

  // on resize, move search and top nav position according to window width
  let resizing = false;
  moveNavigation();
  $(window).on('resize', function() {
    if (!resizing) {
      !window.requestAnimationFrame
        ? setTimeout(moveNavigation, 300)
        : window.requestAnimationFrame(moveNavigation);
      resizing = true;
    }
  });

  // on window scrolling - fix sidebar nav
  let scrolling = false;
  checkScrollbarPosition();
  $(window).on('scroll', function() {
    if (!scrolling) {
      !window.requestAnimationFrame
        ? setTimeout(checkScrollbarPosition, 300)
        : window.requestAnimationFrame(checkScrollbarPosition);
      scrolling = true;
    }
  });

  // mobile only - open sidebar when user clicks the hamburger menu
  sidebarTrigger.on('click', function(event) {
    event.preventDefault();
    $([sidebar, sidebarTrigger]).toggleClass('nav-is-visible');
  });

  // click on item and show submenu
  $('.has-children > a').on('click', function(event) {
    let mq = checkMQ();
      var selectedItem = $(this);
    if (mq == 'mobile' || mq == 'tablet') {
      event.preventDefault();
      if (selectedItem.parent('li').hasClass('selected')) {
        selectedItem.parent('li').removeClass('selected');
      } else {
        sidebar.find('.has-children.selected').removeClass('selected');
        accountInfo.removeClass('selected');
        selectedItem.parent('li').addClass('selected');
      }
    }
  });

  // click on account and show submenu - desktop version only
  accountInfo.children('a').on('click', function(event) {
    let mq = checkMQ();
      var selectedItem = $(this);
    if (mq == 'desktop') {
      event.preventDefault();
      accountInfo.toggleClass('selected');
      sidebar.find('.has-children.selected').removeClass('selected');
    }
  });

  $(document).on('click', function(event) {
    if (!$(event.target).is('.has-children a')) {
      sidebar.find('.has-children.selected').removeClass('selected');
      accountInfo.removeClass('selected');
    }
  });

  // on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents

  sidebar.children('ul').menuAim({
    activate(row) {
      $(row).addClass('hover');
    },
    deactivate(row) {
      $(row).removeClass('hover');
    },
    exitMenu() {
      sidebar.find('.hover').removeClass('hover');
      return true;
    },
    submenuSelector: '.has-children',
  });

  function checkMQ() {
    // check if mobile or desktop device
    return window
      .getComputedStyle(document.querySelector('.cd-main-content'), '::before')
      .getPropertyValue('content')
      .replace(/'/g, '')
      .replace(/"/g, '');
  }

  function moveNavigation() {
    let mq = checkMQ();

    if (mq == 'mobile' && topNavigation.parents('.cd-side-nav').length == 0) {
      detachElements();
      topNavigation.appendTo(sidebar);
      searchForm.removeClass('is-hidden').prependTo(sidebar);
    } else if (
      (mq == 'tablet' || mq == 'desktop') &&
      topNavigation.parents('.cd-side-nav').length > 0
    ) {
      detachElements();
      searchForm.insertAfter(header.find('.cd-logo'));
      topNavigation.appendTo(header.find('.cd-nav'));
    }
    checkSelected(mq);
    resizing = false;
  }

  function detachElements() {
    topNavigation.detach();
    searchForm.detach();
  }

  function checkSelected(mq) {
    // on desktop, remove selected class from items selected on mobile/tablet version
    if (mq == 'desktop') $('.has-children.selected').removeClass('selected');
  }

  function checkScrollbarPosition() {
    let mq = checkMQ();

    if (mq != 'mobile') {
      let sidebarHeight = sidebar.outerHeight();
        var windowHeight = $(window).height();
        var mainContentHeight = mainContent.outerHeight();
        var scrollTop = $(window).scrollTop();

      scrollTop + windowHeight > sidebarHeight &&
      mainContentHeight - sidebarHeight != 0
        ? sidebar.addClass('is-fixed').css('bottom', 0)
        : sidebar.removeClass('is-fixed').attr('style', '');
    }
    scrolling = false;
  }
  $('#cbTerca').prop('checked', true);
  $('#cbQuarta').prop('checked', true);
  $('#cbQuinta').prop('checked', true);

  // $('#modalAdicionarIntervalo').modal({backdrop: 'static', keyboard: false})
});
