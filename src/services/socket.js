import socketIOClient from 'socket.io-client';
import { selectProfissional } from '../store/modules/profissionais/actions';
import store from '../store';

const socket = socketIOClient(process.env.DOUTOR_HOJE_SOCKET);

socket.on('update', socket => {
  // console.log(socket);
  // console.log(JSON.parse(socket.payload));
  //  store.dispatch(selectProfissional(4));
});

export default socket;
