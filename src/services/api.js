import axios from 'axios';
import store from '../store';
import { USER_LOGOUT } from '../assets/consts';

const api = axios.create({
  baseURL: process.env.API_URL,
});

api.interceptors.request.use(config => {
  const { user } = store.getState().auth;

  if (user) {
    config.headers.Authorization = `${user.token_type} ${user.access_token}`;
  }

  // console.log(config, 'request interceptor');

  return config;
});

api.interceptors.response.use(
  response => {
    // console.log(response);

    return response;
  },
  error => {
    if (process.env.NODE_ENV === 'devlopment') {
      console.log({
        status: error.response.status,
        message: error.response.data.message,
      });
    }

    if (error.response.status === 401) {
      store.dispatch({ type: USER_LOGOUT });
    }
    return error.response;
  }
);

export default api;
