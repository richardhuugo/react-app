import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom';
import PrivateRouter from "../../routes/private/privateRouter";
import RoutesCliente from './routes'
  
export default  ({user}) => (
  <Switch>
    {RoutesCliente.map((value,id) => (<PrivateRouter key={id} user={user}   exact path={value.layout}  component={value.component} />)) }

    {RoutesCliente.map((value, id) => {
      if(value.init){
        return <Redirect key={id} from='*' to={value.layout}/>
      }
    })}
   
</Switch>

)