import Dashboard from '../views/dashboard'
import Aluno from  '../views/dashboard'

var routes = [
  {
      init:true,
      path: "",
      name: "Dashboard",
      icon: "ni ni-tv-2 text-primary",
      component: Dashboard,
      layout: "/cliente"
  },
  
  {
    path: "",
    name: "Novo Aluno",
    icon: "ni ni-pin-3 text-orange",
    component: Aluno,
    layout: "/pacotes"
  }
  ];
  export default routes;
  