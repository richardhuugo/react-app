import Dashboard from '../views/dashboard'
import Configuracao from '../views/dashboard'
import Aluno from  '../views/dashboard'
import Planos from  '../views/dashboard'
var routes = [
  {
      init:true,
      path: "",
      name: "Dashboard",
      icon: "ni ni-tv-2 text-primary",
      component: Dashboard,
      layout: "/empresa"
  },
  
  {
    path: "",
    name: "Novo Aluno",
    icon: "ni ni-pin-3 text-orange",
    component: Aluno,
    layout: "/pacotes"
  },
  {
    path: "",
    name: "Alunos",
    icon: "ni ni-pin-3 text-orange",
    component: Aluno,
    layout: "/alunos"
  },
  {
        path: "",
        name: "Funcionarios",
        icon: "ni ni-circle-08 text-blue",
        component: Configuracao,
        layout: "/funcionarios"
  },
  {
        path: "",
        name: "Planos",
        icon: "ni ni-circle-08 text-blue",
        component: Planos,
        layout: "/planos"
  }, 
  
  {
    path: "",
    name: "Financeiro",
    icon: "ni ni-single-02 text-yellow",
    component: Configuracao,
    layout: "/configuracao"
  },
  {
    path: "",
    name: "Configurações",
    icon: "ni ni-bullet-list-67 text-red",
    component: Configuracao,
    layout: "/configuracao"
  }
  ];
  export default routes;
  