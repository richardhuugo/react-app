import React from 'react'
import { withRouter } from 'react-router-dom';

import Header from '../../../components/common/header';



const dashboard = () => {

  return (
    <div className="animated fadeIn">
      <Header />
    </div>
  );
}


export default withRouter(dashboard);
