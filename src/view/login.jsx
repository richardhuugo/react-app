import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter, Link } from 'react-router-dom';
import {
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  
  
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
// core components
import AuthNavbar from "../components/AuthNavbar.jsx";
import AuthFooter from "../components/AuthFooter.jsx";


import { login } from '../store/modules/auth/actions';




class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = { loginMode: true };
    document.title = 'Login';
  }

  changeMode() {
    this.setState({ loginMode: !this.state.loginMode });
  }

  onSubmit(values) {
    const { login } = this.props;
    if (this.state.loginMode) login(values);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <>
        <div className="main-content">
          <AuthNavbar />
          <div className="header bg-gradient-info py-7 py-lg-8">
            <Container>
              <div className="header-body text-center mb-7">
                <Row className="justify-content-center">
                  <Col lg="5" md="6">
                    <h1 className="text-white">Welcome!</h1>
                    <p className="text-lead text-light">
                      Use these awesome forms to login or create new account in
                      your project for free.
                    </p>
                  </Col>
                </Row>
              </div>
            </Container>
            <div className="separator separator-bottom separator-skew zindex-100">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="fill-default"
                  points="2560 0 2560 100 0 100"
                />
              </svg>
            </div>
          </div>
          {/* Page content */}
          <Container className="mt--8 pb-5">
            <Row className="justify-content-center">
                <Col lg="5" md="7">
                  <Card className="bg-secondary shadow border-0">
                    <CardBody className="px-lg-5 py-lg-5">
                      <form role="form" onSubmit={handleSubmit(v => {
                        this.onSubmit(v) 
                      })} >
                        <FormGroup className="mb-3">
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-email-83" />
                              </InputGroupText>
                            </InputGroupAddon>                          
                            <Field
                              component='input'
                              className="form-control"
                              type="number"
                              name="documento"
                              placeholder="Informe seu documento"
                              icon="envelope"
                            />
                            
                          </InputGroup>
                        </FormGroup>
                        <FormGroup>
                          <InputGroup className="input-group-alternative">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            
                            <Field
                              component='input'
                              className="form-control"
                              type="password"
                              name="senha"
                              placeholder="Senha"
                              icon="lock"
                            />
                            
                          </InputGroup>
                        </FormGroup>
                        <div className="text-center">
                          <Button className="my-4" color="primary"  type="submit">
                            Login
                          </Button>
                        </div>
                      </form>
                    </CardBody>
                  </Card>
                  <Row className="mt-3">
                    <Col xs="6">
                      <a
                        className="text-light"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <small>Esqueceu a senha?</small>
                      </a>
                    </Col>
                    
                  </Row>
                </Col>
            </Row>
          </Container>
        </div>
        <AuthFooter />
      </>
      
    );
  }
}

Auth = reduxForm({ form: 'authForm', destroyOnUnmount: false })(Auth);
const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch);
export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(Auth)
);
