// IMPORT PACKAGES
import React from 'react';

import './assets/ReactotronConfig';

import { Provider } from 'react-redux';

import store from './store';

import { AppRouter } from './routes/AppRouter';

// import socket from './services/socket';

export const App = () => (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);
