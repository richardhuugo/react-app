import { all } from 'redux-saga/effects';

 
import profissionais from './profissionais/sagas';
 
import auth from './auth/sagas';

export default function* rootSaga() {
  return yield all([profissionais, auth]);
}
