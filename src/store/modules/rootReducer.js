import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';
 
import auth from './auth/reducer';
import profissionais from './profissionais/reducer';
 

export default combineReducers({
  auth,
  profissionais,
  toastr: toastrReducer,
  form: formReducer,
});
