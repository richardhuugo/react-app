import jwt_decode from 'jwt-decode';
import api from '../../../services/api';
import { USER_FETCH, USER_LOGOUT, userKey } from '../../../assets/consts';

const user = JSON.parse(localStorage.getItem(userKey));

const INITIAL_STATE = {
  user,
  validToken: false,
};
if (user) {
  api.defaults.headers.Authorization = `${user.token_type} ${user.access_token}`;
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_LOGOUT:
      // console.log('LOGOUTreducer');
      localStorage.removeItem(userKey);
      return { ...state, validToken: false, user: null };

    case USER_FETCH: {
      const {   access_token  } = action.payload;
      const {
        email,
        celular,
        perfis,
        status,
        telefone,
        empresa,
        filial,
        funcionario,
        nome_completo,
      } = jwt_decode(access_token);

      console.log(jwt_decode(access_token));

      const user = {
        ...action.payload,
        email,
        celular,
        perfis,
        status,
        telefone,
        empresa,
        filial,
        funcionario,
        nome_completo,
      };

      localStorage.setItem(userKey, JSON.stringify(user));

      return { ...state, user, validToken: true };
    }
    default:
      return state;
  }
};
