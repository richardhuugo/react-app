import { toastr } from 'react-redux-toastr';
import {
  LOGIN_URL,
  USER_LOGOUT,
  USER_FETCH,
  USER_REGISTER,
} from '../../../assets/consts';
// import runActionWithAxios from '../../../../utils/actions';
import runActionWithAxios from '../../../utils/actions';

export function login(values) {
  // return actionPost(LOGIN_URL, { type: USER_FETCH }, values);

  const { documento, senha } = values;
  console.log(LOGIN_URL)
  if (!documento) toastr.error('Documento', 'Preencha o campo de documento');
  else if (!senha) toastr.error('Senha', 'Preencha o campo de senha');
  else
    return runActionWithAxios(LOGIN_URL, 'post', { type: USER_FETCH }, values);
  return [];
}

export function cadastro(values) {
  return runActionWithAxios(LOGIN_URL, 'post', { type: USER_REGISTER }, values);
}

export function logout() {
  return { type: USER_LOGOUT };
}
