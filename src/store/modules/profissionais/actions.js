import {
  GET_PROFISSIONAIS_REQUEST,
  GET_PROFISSIONAIS_SUCCESS,
  HORARIOS_UPDATE_REQUEST,
  HORARIOS_UPDATE_SUCCESS,
  SELECT_PROFISSIONAL,
  // GET_SERVICOS_REQUEST,
  // GET_SERVICOS_SUCCESS,
} from '../../../assets/consts';

export function getProfissionaisRequest(prestadorCnpj) {
  return {
    type: GET_PROFISSIONAIS_REQUEST,
    prestadorCnpj,
  };
}

export function getProfissionaisSuccess(profissionais) {
  return {
    type: GET_PROFISSIONAIS_SUCCESS,
    payload: profissionais,
  };
}

export function updateHorarioRequest(horarioId, horario) {
  return {
    type: HORARIOS_UPDATE_REQUEST,
    horarioId,
    horario,
  };
}
export function updateHorarioSuccess(updatedHoraio) {
  return {
    type: HORARIOS_UPDATE_SUCCESS,
    payload: updatedHoraio,
  };
}

// export function getServicosRequest(profissionalId) {
//   return {
//     type: GET_SERVICOS_REQUEST,
//     profissionalId,
//   };
// }

// export function getServicosSuccess(profissional) {
//   return {
//     type: GET_SERVICOS_SUCCESS,
//     payload: profissional,
//   };
// }

export function selectProfissional(profissionalId) {
  return { type: SELECT_PROFISSIONAL, payload: profissionalId };
}
