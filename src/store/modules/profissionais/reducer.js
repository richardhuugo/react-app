import {
  SELECT_PROFISSIONAL,
  GET_PROFISSIONAIS_SUCCESS,
  HORARIOS_UPDATE_SUCCESS,
  // GET_SERVICOS_SUCCESS,
} from '../../../assets/consts';

const INITIAL_STATE = {
  profissionais: [],
  profissional: {
    servicos: [],
    especialidades: [],
    planos: [],
  },
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PROFISSIONAIS_SUCCESS:
      return {
        ...state,
        profissionais: action.payload,
      };
    case HORARIOS_UPDATE_SUCCESS:
      return {
        ...state,
        horarioUpdated: action.payload,
      };

    case SELECT_PROFISSIONAL:
      return {
        ...state,
        profissional: {
          ...state.profissionais.find(obj => obj.id === action.payload),
        },
      };

    // case GET_SERVICOS_SUCCESS: {
    //   return {
    //     ...state,
    //     profissional: { ...state.profissional, servicos: action.payload },
    //   };
    // }

    default:
      return state;
  }
};
