import { call, put, all, takeLatest } from 'redux-saga/effects';

import api from '../../../services/api';

import {
  GET_PROFISSIONAIS_REQUEST,
  HORARIOS_UPDATE_REQUEST,
  // GET_SERVICOS_REQUEST,
} from '../../../assets/consts';

import {
  getProfissionaisSuccess,
  updateHorarioSuccess,
  // getServicosSuccess,
} from './actions';

//------------------------------------------------------------------------------
function* getProfissionais({ prestadorCnpj }) {
  const url = `/prestadores/${prestadorCnpj}/profissionais`;
  try {
    const response = yield call(api.get, url);
    yield put(getProfissionaisSuccess(response.data));
  } catch (error) {
    // console.log(error.response);
  }
}
//------------------------------------------------------------------------------

function* updateHorario({ horarioId, horario }) {
  const url = `/horarios/${horarioId}`;
  try {
    const response = yield call(api.put, url, horario);
    yield put(updateHorarioSuccess(response.data));
  } catch (error) {
    // console.log(error.response);
  }
}

// function* getServicos({ profissionalId }) {
//   const url = `/profissionais/${profissionalId}/servicos`;

//   try {
//     const response = yield call(api.get, url);
//     yield put(getServicosSuccess(response.data));
//   } catch (error) {
//     console.log(error.response);
//   }
// }

export default all([
  takeLatest(GET_PROFISSIONAIS_REQUEST, getProfissionais),
  takeLatest(HORARIOS_UPDATE_REQUEST, updateHorario),
  // takeLatest(GET_SERVICOS_REQUEST, getServicos),
]);
