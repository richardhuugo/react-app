import { createStore, applyMiddleware, compose } from 'redux';

import thunk from 'redux-thunk';
import promise from 'redux-promise';
import multi from 'redux-multi';
import createSagaMiddleware from 'redux-saga';

// import promiseMiddleware from 'redux-promise-middleware';

import rootReducer from './modules/rootReducer';
import rootSaga from './modules/rootSaga';

const sagaMiddleware = createSagaMiddleware();

const enhancer =
  process.env.NODE_ENV === 'development'
    ? compose(
        console.tron.createEnhancer(),
        // window.__REDUX_DEVTOOLS_EXTENSION__ &&
        //   window.__REDUX_DEVTOOLS_EXTENSION__(),
        applyMiddleware(
          thunk,
          multi,
          promise,
          // promiseMiddleware()
          sagaMiddleware
        )
      )
    : applyMiddleware(
        thunk,
        multi,
        promise,
        sagaMiddleware
        // promiseMiddleware()
      );

const store = createStore(rootReducer, enhancer);

sagaMiddleware.run(rootSaga);

export default store;
