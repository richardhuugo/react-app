// URLS
export const OAPI_URL = process.env.API_URL;
export const LOGIN_URL = `${OAPI_URL}/login`;
export const PERFIL ='perfil_user';

export const CLIENTE = 'CLIENTE';
export const FUNCIONARIO = 'FUNCIONARIO'
export const ADMINISTRADOR = 'ADMINISTRADOR'
export const EMPRESA ='EMPRESA';

export const PROFISSIONAIS_URL = `${OAPI_URL}/profissionais/`;
export const PRESTADORES_URL = `${OAPI_URL}/prestadores/`;
export const STATUSES_URL = `${OAPI_URL}/statuses/`;
export const PACIENTES_URL = `${OAPI_URL}/pacientes/`;
export const AGENDAMENTOS_URL = `${OAPI_URL}/agendamentos/`;

export const userKey = '_keyuser'; 

// CONSTANTES USADAS REDUX - ACTION TYPES
export const USER_FETCH = 'USER_FETCH';
export const USER_REGISTER = 'USER_REGISTER';
export const USER_LOGOUT = 'USER_LOGOUT';
export const AGENDAMENTOS_FETCHED_FROM_PERIOD =
  'AGENDAMENTOS_FETCHED_FROM_PERIOD';

export const PRESTADOR_REGISTER = 'PRESTADOR_REGISTER';

export const GET_PRESTADOR_REQUEST = '@prestador/GET_REQUEST';
export const GET_PRESTADOR_SUCCESS = '@prestador/GET_SUCCESS';
export const UPDATE_PRESTADOR_REQUEST = '@prestador/UPDATE_REQUEST';
export const UPDATE_PRESTADOR_SUCCESS = '@prestador/UPDATE_SUCCESS';

export const GET_PROFISSIONAIS_REQUEST = '@profissionais/GET_REQUEST';
export const GET_PROFISSIONAIS_SUCCESS = '@profissionais/GET_SUCCESS';
export const SELECT_PROFISSIONAL = '@profissionais/SELECT';

export const HORARIOS_UPDATE_REQUEST = '@profissionais/UPDATE_HORARIO_REQUEST';
export const HORARIOS_UPDATE_SUCCESS = '@profissionais/UPDATE_HORARIO_SUCCESS';

export const GET_SERVICOS_REQUEST = '@profissionais/GET_SERVICOS_REQUEST';
export const GET_SERVICOS_SUCCESS = '@profissionais/GET_SERVICOS_SUCCESS';

export const AGENDAMENTO_UPDATE = '@agendamentos/UPDATE_REQUEST';
export const EVENTS_CREATE = '@agendamentos/EVENTS_CREATE';
export const GET_STATUSES_REQUEST = '@agendamentos/STATUSES_LIST_REQUEST';
export const STATUSES_FETCH_SUCCESS = '@agendamentos/STATUSES_FETCH_SUCCESS';
export const AGENDAMENTOS_FROM_PERIOD_REQUEST = '@agendamentos/PERIOD_REQUEST';
export const AGENDAMENTOS_FROM_PERIOD_SUCCESS = '@agendamentos/PERIOD_SUCCESS';
export const AGENDAMENTOS_CURRENT_WEEK_REQUEST = '@agendamentos/WEEK_REQUEST';
export const AGENDAMENTOS_CURRENT_WEEK_SUCCESS = '@agendamentos/WEEK_SUCCESS';
export const AGENDAMENTOS_PROFISSIONAL_REQUEST =
  '@agendamentos/PROFISSIONAL_REQUEST';

export const GET_PACIENTES_REQUEST = '@pacientes/GET_PACIENTES_REQUEST';
export const GET_PACIENTES_SUCCESS = '@pacientes/GET_PACIENTES_SUCCESS';
