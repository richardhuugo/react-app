import Reactotron from 'reactotron-react-js';
import { reactotronRedux } from 'reactotron-redux';
import reactotronSaga from 'reactotron-redux-saga';

if (process.env.NODE_ENV === 'development') {
  // console.log(process.env);
  const tron = Reactotron.configure()
    .use(reactotronRedux())
    .connect();

  console.tron = tron;
  tron.clear();
}
