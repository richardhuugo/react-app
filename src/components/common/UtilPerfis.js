import React from 'react'
import {DropdownItem} from "reactstrap";
import { PERFIL } from '../../assets/consts';
  
export const popularPerfis = (perfis, db) => {
  let configurado = db;
 return  perfis.map((value, id) => {
    return (
      <DropdownItem key={id} onClick={() => {atualizarPerfil(value,configurado)}} >{value.perfil}</DropdownItem>
    )
  })
}

export const atualizarPerfil = (value,configurado) => {
  let item_info ='item_info';
  if(value.perfil !== configurado){
    localStorage.removeItem(PERFIL)
    localStorage.setItem(PERFIL,value.perfil)
    localStorage.removeItem(item_info)
    localStorage.setItem(item_info,JSON.stringify(value))
    location.reload()
  }
}