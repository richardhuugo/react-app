import styled from 'styled-components';
import { DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export const StyledDropdownItem = styled(DropdownItem)`
  color: #569;
  background: #824;
  font-size: 12px;
`;
