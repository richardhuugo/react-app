import React from 'react';
import { withRouter } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { selectProfissional } from '../../store/modules/profissionais/actions';

function TabsProfissionais() {
  const profissionais = useSelector(state => state.profissionais.profissionais);
  const selectedProfissional = useSelector(
    state => state.profissionais.profissional
  );
  const dispatch = useDispatch();

  function tabsProfissionais() {
    return profissionais.map(
      ({ nm_primario, nm_secundario, crm, id }, index) => {
        const nome = `${nm_primario} ${nm_secundario}`;
        const profissionalRef = `#profissional${crm}`;

        return (
          <li key={`${id}`} className="nav-item">
            <a
              className={
                id === selectedProfissional.id ? 'nav-link active' : 'nav-link'
              }
              onClick={() => {
                dispatch(selectProfissional(id));
              }}
              data-toggle="tab"
              role="tab"
              aria-controls="profissional"
              href={profissionalRef}
              aria-selected="true"
            >
              {nome}
            </a>
          </li>
        );
      }
    );
  }

  return (
    <ul className="nav nav-tabs" id="tabProfissionais" role="tablist">
      {tabsProfissionais()}
    </ul>
  );
}

export default withRouter(TabsProfissionais);
