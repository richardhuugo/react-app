import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';
import "./assets/vendor/nucleo/css/nucleo.css";
import "./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css";
import "./assets/css/argon-dashboard-react.min.css";

ReactDOM.render(<App />, document.getElementById('root'));
