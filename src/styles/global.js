// import styled from 'styled-components';

// Base ------------------------------------------------------------------------
export const silver = '#f6f7fa';
export const lighterGray = '#dddddd';
export const lightGray = '#cdced3';
export const gray = '#b1b3bb';
export const darkSilver = '#8c939e';
export const darkGray = '#5c616c';
export const dark = '#23252f';

// Tema ------------------------------------------------------------------------
export const mainBlue = '#1b71b9';
export const mainBlueHover = '#165e99';
export const mainRed = '#be1421';
export const mainRedHover = '#9e111b';

// Auxiliares
export const success = '#30b91b';
export const warning = '#e5d330';
export const warningHover = '#bfb028';
