import moment from 'moment';
import { format } from 'date-fns';

const getperiodFromToday = timeInMonths => {
  const now = new Date();
  const before = new Date();
  const after = new Date();

  before.setMonth(now.getMonth() - timeInMonths);
  after.setMonth(now.getMonth() + timeInMonths);

  return {
    start: format(before, 'YYYY-MM-DD'),
    end: format(after, 'YYYY-MM-DD'),
  };
};

const parseHorarioStrToMoment = horarios =>
  horarios &&
  horarios.map(horario => {
    const { hr_entrada, hr_saida } = horario;
    const horarioInt = horario;
    horarioInt.hr_entrada = moment(hr_entrada, 'HH:mm');
    horarioInt.hr_saida = moment(hr_saida, 'HH:mm');
    return horarioInt;
  });

const parseMomentToStr = momentDate => momentDate.format('HH:mm:ss');

export { getperiodFromToday, parseHorarioStrToMoment, parseMomentToStr };
