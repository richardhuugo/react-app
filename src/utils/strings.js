const excludes = ['de', 'da'];

const capitalizeString = str =>
  str
    .toLowerCase()
    .split(' ')
    .map(name => {
      let nameCapitalized = name;
      if (excludes.indexOf(name) === -1) {
        nameCapitalized = name.charAt(0).toUpperCase() + name.substring(1);
      }
      return nameCapitalized;
    })
    .join(' ');

export { capitalizeString };
