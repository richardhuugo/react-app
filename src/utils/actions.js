/* eslint-disable no-nested-ternary */
import { toastr } from 'react-redux-toastr';
import axios from 'axios';
import { USER_LOGOUT } from '../assets/consts';

const logout = { type: USER_LOGOUT, payload: null };

// AUXILIARY FUNCTONS ↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓
const mapMultipleActions = (arrayOfActions, data) => {
  const actions = [];

  arrayOfActions.map(action => {
    actions.push({
      type: action.type,
      payload: action.payload
        ? action.payload
        : action.functionPayload
        ? action.functionPayload(data)
        : data,
    });
  });
  return actions;
};

const checkIfTokenIsExpired = response => {
  if (response && response.status === 401) return true;
  return false;
};
// AUXILIARY FUNCTONS ↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑

const actionAxiosPost = (url, actionObject, values) => {
  return dispatch =>
    axios
      .post(url, values)
      .then(resp => {
        const actions = mapMultipleActions(actionObject, resp.data);
        dispatch(actions);
      })
      .catch(e => {
        console.log({url, values,e})
        if (e.response) {
          if (checkIfTokenIsExpired(e.response)) dispatch(logout);
          toastr.error('Erro', e.response.data.message);
          console.warn('Post - Erro', e.response);
        } else {
          toastr.error('Erro', e.message);
          console.warn('Post - Erro', { e });
        }
      });
};

const actionAxiosGet = (url, arrayOfActions, values) => dispatch =>
  axios
    .get(url, { params: { ...values } })
    .then(resp => {
      const actions = mapMultipleActions(arrayOfActions, resp.data);
      dispatch(actions);
    })
    .catch(e => {
      if (e.response) {
        if (checkIfTokenIsExpired(e.response)) dispatch(logout);
        toastr.error('Erro', e.response.data.message);
        console.warn('Get - Erro', e.response);
      } else {
        toastr.error('Erro', e.message);
        console.warn('Get - Erro', { url, e });
      }
    });

const actionAxiosPut = (url, arrayOfActions, values) => dispatch =>
  axios
    .put(url, values)
    .then(resp => {
      const actions = mapMultipleActions(arrayOfActions, resp.data);
      dispatch(actions);
    })
    .catch(e => {
      if (e.response) {
        if (checkIfTokenIsExpired(e.response)) dispatch(logout);
        toastr.error('Erro', e.response.data.message);
        console.warn('Put - Erro', e.response.data);
      } else {
        toastr.error('Erro', e.message);
        console.warn('Put - Erro', { url, e });
      }
    });

// EXPORTED FUNCTION ↓ - ↓ - ↓ - ↓ - ↓ - ↓ - ↓ ----------------------------------------------------
const runActionWithAxios = (url, requestType, actionObject, values = null) => {
  let arrayOfActions = actionObject;
  if (!(actionObject.length && actionObject.length > 1))
    arrayOfActions = [actionObject];

  switch (requestType) {
    case 'get':
      return actionAxiosGet(url, arrayOfActions, values);
    case 'post':
      return actionAxiosPost(url, arrayOfActions, values);
    case 'put':
      return actionAxiosPut(url, arrayOfActions, values);
    default:
      console.error(`---Erro: Tipo inválido: requestType: ${requestType}`);
      return undefined;
  }
};
// ------------------------------------------------------------------------------------------------

export default runActionWithAxios;
