import { toastr } from 'react-redux-toastr';
import api from '../services/api';
import store from '../store';

function errorResponses(response) {
  const { erro, message } = response.data;
  console.log(erro, message);

  if (erro) toastr.error('Ops!', erro[0] ? erro[0] : 'Erro.');
}

export async function runPost(url, params) {
  const { auth } = store.getState();
  let config = null;
  if (auth.user) {
    config = {
      headers: {
        Authorization: `${auth.user.token_type} ${auth.user.access_token}`,
      },
    };
  }
  try {
    const response = await api.post(url, params, config);
    return response;
  } catch (error) {
    errorResponses(error.response);
    return error.response;
  }
}

export async function runGet(url, params) {
  const { auth } = store.getState();
  let config = null;
  if (auth.user) {
    config = {
      headers: {
        Authorization: `${auth.user.token_type} ${auth.user.access_token}`,
      },
    };
  }
  try {
    const response = await api.get(url, { ...config, params });
    return response;
  } catch (error) {
    errorResponses(error.response);
    return error.response;
  }
}
