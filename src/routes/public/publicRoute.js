import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr';
 
import Auth from '../../view/login'
const publicRoute = () => (
  <div>
    <Switch>
      <Route path="/login" component={Auth} />
      <Redirect to="/login" />
    </Switch>
    <ReduxToastr
      timeOut={4000}
      newestOnTop={false}
      preventDuplicates
      position="top-right"
      transitionIn="fadeIn"
      transitionOut="fadeOut"
      progressBar
    />
  </div>
);

export default publicRoute;
