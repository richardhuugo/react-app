import React from 'react';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PrivatesRoutes from './private/routes';
import PublicRoute from './public/publicRoute';
// import api from '../services/api';

function AuthOrApp() {
  const auth = useSelector(state => state.auth);
  return auth.user ? <PrivatesRoutes user={auth.user} /> : <PublicRoute />;
}

export default withRouter(AuthOrApp);
