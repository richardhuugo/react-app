import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { Container } from "reactstrap";
import ReduxToastr from 'react-redux-toastr';
import PrivateRouter from './privateRouter';
import Header from '../../components/common/header';
import NavBar from '../../components/common/navbar'
import Sidebar from  '../../components/common/sidebar'
// import Sidebar from '../../components/common/sidebar';
 
// ROUTES EMPRESA
import RoutesEmpresa from '../../view/empresa/routes/index'
import RoutesCliente from '../../view/cliente/routes/index'
// VIEWS
import EmpresaView from '../../view/empresa/index' 
import ClienteView from '../../view/cliente/index'
import { userKey, PERFIL, ADMINISTRADOR,CLIENTE ,EMPRESA, FUNCIONARIO} from '../../assets/consts';
import { atualizarPerfil } from '../../components/common/UtilPerfis';
export default class Routes extends React.Component{
   verificarToken = () => {
    const local = localStorage.getItem(userKey)
    if(local){
      return (JSON.parse(local))
    }else{
      return []
    }
  }
  padraoPerfil = (perfil) => {
    let perfis = [];
    perfil.perfis.map( data => {
      perfis.push({
        perfil:data.perfil_id.perfil,
        status:data.status,
        id:data._id
      })
    })
    return perfis
  }
  padraoSelecionado = () => {
    let perfil = this.padraoPerfil(this.verificarToken())
    if(localStorage.getItem(PERFIL)==''){
      atualizarPerfil(perfil[0],'')
    }
  }
  renderView = () => {
    switch(localStorage.getItem(PERFIL)){
      case ADMINISTRADOR:
        return  <EmpresaView />     
      case CLIENTE:
        return  <ClienteView />  
      case EMPRESA:
        return  <EmpresaView /> 
      case FUNCIONARIO:
        return  <EmpresaView /> 
      default:
          return  <EmpresaView /> 
    }
    
  }
  renderRoutes = () => {
    switch(localStorage.getItem(PERFIL)){
      case ADMINISTRADOR:
        return  []
      case CLIENTE:
        return RoutesCliente 
      case EMPRESA:
        return  RoutesEmpresa
      case FUNCIONARIO:
        return  []
      default:
        return  []
    }
  }
  render(){
    return(
      <div className="animated fadeIn">
        {this.padraoSelecionado()}
        <Sidebar {...this.props} routes={this.renderRoutes()}   perfis={this.padraoPerfil(this.verificarToken())}  logo={{ innerLink: "/admin/index",imgSrc: "", imgAlt: "..."}} />     
        <div className="main-content"  ref="mainContent">
          <NavBar {...this.props} perfis={this.padraoPerfil(this.verificarToken())}      brandText="Admin" /> 
          {this.renderView()}
          <Container fluid />                                         
        </div>
        <ReduxToastr
          timeOut={4000}
          newestOnTop={false}
          preventDuplicates
          position="top-right"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar
        />
      </div>
    )
  }
}
 