import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import axios from 'axios';

const PrivateRouter = ({ user, component: Component, ...rest }) => {
  if (user) {
    axios.defaults.headers.common.authorization = `${user.token_type} ${user.access_token}`;
  }
  
  return (
    <Route
      {...rest}
      render={props =>
        !user ? (
          <Component {...props}   />
        ) : (
          <Redirect
            to={{ pathname: '/login', state: { from: props.location } }}
          />
        )
      }
    />
  );
};
export default PrivateRouter;
