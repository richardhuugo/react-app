import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import AuthOrApp from './AuthOrApp';
import { createBrowserHistory } from "history";

const customHistory = createBrowserHistory();

export const AppRouter = () => (
  <Router history={customHistory}>
    <AuthOrApp />
  </Router>
);
